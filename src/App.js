import React, { Fragment } from 'react';
import { BrowserRouter, Routes, Route } from "react-router-dom";

import Login from './components/Login/Login';
import Layout from './components/Layout/Layout';
import AllCards from './pages/AllCards';
import CardDetail from './pages/CardDetail';
import Decks from './pages/Decks';
import DeckDetail from './pages/DeckDetail';
import PageNotFound from './pages/PageNotFound';

const App = () => {

    return (
        <Fragment>
            <BrowserRouter>
                <Routes>
                    <Route path="/" element={<Login />} />
                    <Route path="/" element={<Layout />}>
                        <Route path='all-cards' element={<AllCards />}></Route>
                        <Route path='/all-cards/:cardId/:cardName' exact element={<CardDetail/>} />
                        <Route path='/decks' element={<Decks />} />
                        <Route path='/decks/:deckName' exact element={<DeckDetail />} />
                        <Route path="*" element={<PageNotFound />} />
                    </Route>
                </Routes>
            </BrowserRouter>
        </Fragment>
    );
}

export default App
