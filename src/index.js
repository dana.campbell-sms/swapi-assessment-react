import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import store from './store';
import './index.css';
import { Provider } from 'react-redux';
import { fetchAsyncCards } from './store/card-slice';

const root = ReactDOM.createRoot(document.getElementById('root'));
store.dispatch(fetchAsyncCards());
root.render(
    <>
        <Provider store={store}>
            <App />
        </Provider>
    </>
);