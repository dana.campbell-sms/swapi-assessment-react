import React from 'react';
// import { NavLink } from 'react-router-dom';
import './MenuItem.css';

const MenuItem = ({imgSrc, alt, title, outline, controls, onClick, style}) => {
    const menuItemStyle = outline ? 'menu-item-border': 'menu-item';
    return (
        <button className={menuItemStyle} onClick={onClick} style={style}>
            {
                controls ? <></> : (
                    <img src={imgSrc} alt={alt}/>
                )
            }
            <span>{title}</span>
        </button>
    )
}

export default MenuItem