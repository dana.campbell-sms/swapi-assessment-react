import React, { Fragment } from 'react';
import { NavLink, useNavigate } from 'react-router-dom';

import './Menu.css';
import MenuItem from './MenuItem';
import Card from '../../../assets/Card.svg';
import Deck from '../../../assets/Deck.svg';
import Underline from '../../UI/Underline';

const Menu = () => {
    var user = localStorage.getItem('username');
    const navigate = useNavigate();

    const LogoutHandler = () => {
        window.localStorage.removeItem('isLogged in');
        window.localStorage.removeItem('username');
        navigate('/');
    }

    return (
        <Fragment>
            <nav>
                <div className='nav__left'>
                    <NavLink  to='/all-cards' className={(navData) => navData.isActive ? "active" : "" }>
                        <MenuItem imgSrc={Card} alt='card' title='All Cards'/>
                    </NavLink>
                    <NavLink to='/decks'className={(navData) => navData.isActive ? "active" : "" }>
                        <MenuItem imgSrc={Deck} alt='deck' title='Decks'  />
                    </NavLink>
                </div>
                <div className='nav__middle' to='decks'>
                    <h1><span>SW</span>-API Deck Builder</h1>
                </div>
                <div>
                    <MenuItem title={user} onClick={LogoutHandler} outline/>
                </div>
            </nav>
            <Underline  bg={400}/>
        </Fragment>
    )
}

export default Menu