import React from 'react';
import Menu from './Menu/Menu';
import { Outlet } from 'react-router-dom';


const Layout = () => {
    return (
        <div className='container'> 
            <Menu />
            <main>
                <Outlet />
            </main>
        </div>
    )
}

export default Layout