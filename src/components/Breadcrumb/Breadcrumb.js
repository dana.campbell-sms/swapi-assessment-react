import React from 'react';
import { Link } from 'react-router-dom';
import './Breadcrumb.css';
import RightArrow from '../../assets/RightArrow.svg'

const Breadcrumb = ({crumb1, crumb1Link, crumb2, color1, color2}) => {

    return (
        <div className='breadcrumb'>
            <Link style={{ color: color1 }} to={crumb1Link}>{crumb1}</Link>
            <img src={RightArrow} alt='right arrow'/>
            <span style={{ color: color2}}>{crumb2}</span>
        </div>
    )
}

export default Breadcrumb