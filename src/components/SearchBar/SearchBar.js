import React, { useRef } from 'react';
import './SearchBar.css';
import Search from '../../assets/Search.svg';

const SearchBar = ({term, onSearchTerm}) => {
    const searchInput = useRef('');

    const searchInputHandler = () => {
        onSearchTerm(searchInput.current.value);
    }
    
    return (
        <fieldset className='card-control__search'>
            <input type='text' 
                placeholder='Search' 
                onChange={searchInputHandler} 
                value={term || ''} 
                ref={searchInput} />
            <img src={Search} alt='search' />
        </fieldset>
    )
}

export default SearchBar