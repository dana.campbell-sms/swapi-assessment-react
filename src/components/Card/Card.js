import React, { Fragment } from 'react';
import { Link, useParams } from 'react-router-dom';

import { useDispatch } from 'react-redux';
import { deckActions } from '../../store/deck-slice';
import useFetchDetails from '../../hooks/useFetchDetails';

import './Card.css';
import CardWrapper from './CardBuilder/CardWrapper';
import CardTop from './CardBuilder/CardTop';
import CardBody from './CardBuilder/CardBody';
import CardBodyContent from './CardBuilder/CardBodyContent';
import CardInfo from './CardBuilder/CardInfo';
import Underline from '../UI/Underline';
import FormToggler from '../FormToggler/FormToggler';
import SelectDeck from '../Deck/SelectDeck';
import IconButton from '../UI/IconButton';

import Add from '../../assets/Add.svg';
import CardImg from '../../assets/Card.svg';
import Female from '../../assets/Female.svg';
import Male from '../../assets/Male.svg';
import Homeworld from '../../assets/Planet.svg';
import Starship from '../../assets/Starships.svg';
import Vehicle from '../../assets/Vehicles.svg';
import Remove from '../../assets/Remove.svg';
import Move from '../../assets/Move.svg';

const Card = (props) => {
	const { cardName , gender, species, birth_year, homeworld, vehicles, starships, id, page, style } = props;
	
	const { cardData : homeworldData } = useFetchDetails(homeworld);
	const { cardData : speciesData } = useFetchDetails(species);
	const { cardData : vehiclesData } = useFetchDetails(vehicles);
	const { cardData : starshipsData } = useFetchDetails(starships);

	const vehicleInfo = (
		vehiclesData.length > 1 ? 
            vehiclesData.map((vehicle) => {
                return <CardInfo imgSrc={Vehicle} title = "Vehicles" info={vehicle}/>
            }) :
             <CardInfo imgSrc={Vehicle} title = "Vehicles" info={vehiclesData}/>
	)

	const starshipInfo = (
		starshipsData.length > 1 ? 
		    starshipsData.map((starship) => {
		        return <CardInfo imgSrc={Vehicle} title = "Starships" info={starship}/>
		    }) :
		    <CardInfo imgSrc={Starship} title = "Starships" info={starshipsData}/>
	)
    
	const genderImg = gender === 'male'? Male : gender === 'female'? Female : '';
	let cardNameParam = cardName.replace(/ /g,"-").toLowerCase();
	let cardLink = `/all-cards/${id}/${cardNameParam}`;

	let {deckName} = useParams();
	const dispatch = useDispatch();

	const deleteCardFromDeckHandler = () => {
		dispatch(deckActions.deleteCardFromDeck({
			id,
			name : deckName.replace(/-/g," "),
		}))
	
	}
	
	return (
		<CardWrapper id={id} page={page} style={style}>
			<CardTop src={CardImg} style={{background: 'var(--gray500)'}}>
				{page === 'card-detail' && <span className='card__top-title' to={cardLink}>{cardName}</span>}
				{page === 'all-cards' &&
					<Fragment>
						<Link className='card__top-title' to={cardLink}>{cardName}</Link>
						<FormToggler
							src={Add} 
							alt='plus sign' 
							style={{position: 'absolute', top: '16px', right: '16px'}}
							Component={SelectDeck}
							page='all-cards'
							cardId={id}/>
					</Fragment>
				}

				{page === 'deck-detail' && (
					<Fragment>
						<Link className='card__top-title' to={cardLink}>{cardName}</Link>
						<FormToggler
							src={Move} 
							alt='plus sign' 
							style={{position: 'absolute', top: '16px', right: '60px'}}
							Component={SelectDeck}
							page='deck-detail'
							cardId={id} />
						<IconButton 
							 src={Remove}
							 alt='delete'
							 onClick={deleteCardFromDeckHandler}
							 style={{position: 'absolute', top: '16px', right: '16px', background: 'var(--white)'}}
						/>
					</Fragment>
				)}
            </CardTop>
			<CardBody>
				<CardInfo 
					primary imgSrc={genderImg} title={birth_year} 
					info = {speciesData ? speciesData : 'n/a'}
				/>
				<Underline  bg={300}/>
				<CardBodyContent>
				{ page === 'card-detail' ? 
					<>
						<CardInfo imgSrc={Homeworld} title = "Homeworld" info={homeworldData}/>
						{vehicleInfo}
						{starshipInfo}
					</>
				 	:
					<>
						<CardInfo imgSrc={Homeworld} title = "Homeworld" info={homeworldData}/>
						<CardInfo imgSrc={Vehicle} title = "Vehicles" info={vehicles.length}/>
						<CardInfo imgSrc={Starship} title = "Starships" info={starships.length}/>
					</>
				}
				</CardBodyContent>
			</CardBody>
		</CardWrapper>
	)
}

export default Card