import React from 'react';

import './CardControl.css';
import Add from '../../assets/Add.svg';

import SearchBar from '../SearchBar/SearchBar';
import CardFilter from './CardFilter';
import FormToggler from '../FormToggler/FormToggler';
import CreateDeck from '../Deck/CreateDeck';

const CardControl = ({term, onSearchTerm, onSortCards, setActiveSortOption, deck}) => {
    return (
        <div className='card-control'>
            <SearchBar  term={term} onSearchTerm={onSearchTerm} />
            {
                deck ?
                <div className='card-control__deck'>
                   <FormToggler
                    src={Add} 
                    alt='plus sign' 
                    style={{boxShadow: '0px 2px 4px rgba(0, 0, 0, 0.08), 0px 4px 6px rgba(0, 0, 0, 0.12)'}}
                    Component={CreateDeck}
                    toggle
                    /> 
                </div>
                : 
                <CardFilter onSortCards={onSortCards} setActiveSortOption={setActiveSortOption} /> 
            }

        </div> 
    )
}

export default CardControl