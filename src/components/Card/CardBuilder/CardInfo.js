import React from 'react';
import './CardInfo.css';

const CardInfo = (props) => {
    let detailStyle = props.primary ? 'card-detail1' : 'card-detail2';
    return (
        <div className={`${detailStyle}`}>
            <div>
                <img src={props.imgSrc} alt="" />
                <span>{props.title}</span>
            </div>
            <p>{props.info}</p>
        </div>
    )
}

export default CardInfo