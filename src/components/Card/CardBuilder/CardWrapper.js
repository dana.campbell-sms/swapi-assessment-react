import React from 'react';
import './CardWrapper.css';

const CardWrapper = ({id, children,style}) => {
    return (
        <div className='card' id={id} style={style}>
            {children}
        </div>
    )
}

export default CardWrapper