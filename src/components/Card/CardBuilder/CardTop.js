import React from 'react';
import './CardTop.css';

const CardTop = ({children, src, style, classes}) => {
    return (
        <div className={`card__top ${classes}`} style={style}>       
            <img src={src} alt='Card' />
            {children}
        </div>
    )
}

export default CardTop