import React from 'react';
import './CardBodyContent.css';

const CardBodyContent = ({children}) => {
  return (
    <div className='card__body-content'>{children}</div>
  )
}

export default CardBodyContent