import React from 'react';
import './CardBody.css';

const CardBody = ({children}) => {
  return (
    <div className='card__body'>{children}</div>
  )
}

export default CardBody