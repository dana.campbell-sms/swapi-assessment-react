import React from 'react';
import MenuItem from '../Layout/Menu/MenuItem';
import './CardFilter.css';

const CardFilter = ({onSortCards, setActiveSortOption}) => {
    return (
        <div className='card-control__filter'>
            <MenuItem title='A to Z' style={{ 
                backgroundColor : setActiveSortOption === 'asc'? 'var(--white)': '', 
                border: setActiveSortOption === 'asc'? '1px solid var(--gray400)': ''}} onClick={()=> onSortCards('asc')} controls/>

            <MenuItem title='Youngest' style={{ 
                backgroundColor : setActiveSortOption === 'youngest'? 'var(--white)': '', 
                border: setActiveSortOption === 'youngest'? '1px solid var(--gray400)': ''}} onClick={()=> onSortCards('youngest')} controls/>

            <MenuItem title='Eldest' style={{ 
                backgroundColor : setActiveSortOption === 'eldest'? 'var(--white)': '', 
                border: setActiveSortOption === 'eldest'? '1px solid var(--gray400)': ''}} onClick={()=> onSortCards('eldest')} controls/> 
        </div>
    )
}

export default CardFilter