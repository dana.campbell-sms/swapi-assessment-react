import React, { Fragment, useState, useEffect } from 'react';

import NotFound from '../NotFound/NotFound';
import LoadingSpinner from '../UI/LoadingSpinner';

import ReactPaginate from 'react-paginate';

import './CardList.css';
import Robot from '../../assets/Robot.svg';
import RightArrow from '../../assets/RightArrow.svg';
import LeftArrow from '../../assets/LeftArrow.svg';
import More from '../../assets/More.png';


const CardList = ({data, status, termFound, term, Component, page}) => {
    const [currentItems, setCurrentItems] = useState([]);
    const [pageCount, setPageCount] = useState(0);
    const [itemOffset, setItemOffset] = useState(0);
    
    let itemsPerPage = 10;
    useEffect(() => {
        const endOffset = itemOffset + itemsPerPage;
        setCurrentItems(data.slice(itemOffset, endOffset));
        setPageCount(Math.ceil(data.length / itemsPerPage));
    }, [itemOffset, itemsPerPage, data]);
  
    const handlePageClick = (event) => {
        const newOffset = (event.selected * itemsPerPage) % data.length;
        setItemOffset(newOffset);
    };

    if (status === 'loading') {
        return (
            <LoadingSpinner />
        );
    }
  
    if (status === 'error') {
        return <NotFound imgSrc={Robot} title='Error' text='Something went wrong' alt='robot' />;
    }

    if(!termFound) {
        return <NotFound imgSrc={Robot} title={`Search Results for '${term}' is empty`} text='Try searching for something else' alt='robot' />;
    }
    
    return (
        <Fragment>
            <div className='card-list'>
                {currentItems.map((card, i) => (
                    <Component key={i} {...card} page={page}/>             
                ))}
            </div>

            <ReactPaginate
                breakLabel={<img src={More} alt='more' />}
                breakClassName='pagination__break'
                nextLabel={<img src={RightArrow} alt='right arrow' />}
                nextClassName='pagination__control'
                disabledClassName='disabled'
                onPageChange={handlePageClick}
                marginPagesDisplayed={0}
                pageRangeDisplayed={5}
                pageCount={pageCount}
                previousLabel={<img src={LeftArrow} alt='left arrow' />}
                previousClassName='pagination__control'
                renderOnZeroPageCount={null}
                containerClassName='pagination'
                pageClassName='pagination__item'
                activeClassName='item-active'
                pageLinkClassName='pagination__item-link'
            />
 
        </Fragment>
    )
}

export default CardList