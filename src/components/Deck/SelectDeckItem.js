import React from 'react';
import { useDispatch } from 'react-redux';
import { deckActions } from '../../store/deck-slice';
import './SelectDeckItem.css';

const SelectDeckItem = ({title, cardId, deckName, page, onClickRemove}) => {
    const dispatch = useDispatch();
    
    const addToDeckHandler = () => {
        dispatch(deckActions.addCardToDeck({
            name : title,
            cardId: cardId, 
        }))
        onClickRemove(false);
    }

    const transferCardHandler = () => {
        dispatch(deckActions.deleteCardFromDeck( {
            name: deckName[0],
            id: cardId,
        }))

        dispatch(deckActions.addCardToDeck({
            name: title,
            cardId: cardId,
        }))
        onClickRemove(false);
    }

  return (
    <div className='deck-selection__list-item' onClick={page==='deck-detail' ? transferCardHandler : addToDeckHandler}>{title}</div>
  )
}

export default SelectDeckItem