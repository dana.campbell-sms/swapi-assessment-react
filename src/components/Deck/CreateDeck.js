import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { deckActions } from '../../store/deck-slice';

import IconButton from '../UI/IconButton';
import RebelAlliance from '../../assets/RebelAlliance.svg'
import JediOrder from '../../assets/JediOrder.svg'
import GalaticEmpire from '../../assets/GalaticEmpire.svg'
import NoFaction from '../../assets/NoFaction.svg'
import './CreateDeck.css';

const CreateDeck = ({onClickRemove}) => {
    const [activeFaction, setActiveFaction] = useState('');
    const [deckName, setDeckName] = useState('');
    const dispatch = useDispatch();

    const onSubmitHandler = (e) => {
        e.preventDefault();

        if (activeFaction === '') {
            return;
        }
        
        dispatch(
            deckActions.addNewDeck({
            //   name: deckName,
              name: deckName.replace(/-/g," "),
              faction : activeFaction,
            })
        );
        onClickRemove(false);
        setActiveFaction('');
        setDeckName('');
    }

    return (
        <div className='create-deck'>
            <div className='create-deck__factions'>
                <span>Faction</span>
                <div>
                    <IconButton src={RebelAlliance} alt='Rebel Alliance' onClick={() => setActiveFaction('Rebel Alliance')} 
                    classes={activeFaction === 'Rebel Alliance'? 'faction-active' : 'faction-default'}/>

                    <IconButton src={JediOrder} alt='Jedi Order' onClick={() => setActiveFaction('Jedi Order')}
                    classes={activeFaction === 'Jedi Order'? 'faction-active' : 'faction-default'}/>

                    <IconButton src={GalaticEmpire} alt='Galatic Empire' onClick={() => setActiveFaction('Galatic Empire')} 
                    classes={activeFaction === 'Galatic Empire'? 'faction-active' : 'faction-default'}/>

                    <IconButton src={NoFaction} alt='no faction' onClick={() => setActiveFaction('none')} 
                    classes={activeFaction === 'none'? 'faction-active' : 'faction-default'}/>
                   
                </div>
            </div>
            <form className='create-deck__form' onSubmit={onSubmitHandler}>
                <input 
                    type='text' 
                    placeholder='Enter Deck Name' 
                    id='deck-name' 
                    value={deckName} 
                    onChange={(e) => setDeckName(e.target.value)} required/>
                <label className='create-deck__name' htmlFor='deck-name'>Deck Name</label>
            </form>
        </div>
    )
}

export default CreateDeck