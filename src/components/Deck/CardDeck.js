import React from 'react';
import { Link } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { deckActions } from '../../store/deck-slice';

import CardTop from '../Card/CardBuilder/CardTop';
import CardBody from '../Card/CardBuilder/CardBody';
import CardWrapper from '../Card/CardBuilder/CardWrapper';
import IconButton from '../UI/IconButton';

import RebelAllianceWatermark from '../../assets/RebelAllianceWatermark.svg';
import JediOrderWatermark from '../../assets/JediOrderWatermark.svg';
import GalaticEmpireWatermark from '../../assets/GalaticEmpireWatermark.svg';
import NoFactionWatermark from '../../assets/NoFactionWatermark.svg';

import './CardDeck.css';
import Deck from '../../assets/Deck.svg';
import Delete from '../../assets/Remove.svg';

const CardDeck = ({name, totalCards, faction}) => {
    const dispatch = useDispatch();
    let deckNameParam = name.replace(/ /g,"-").toLowerCase();
    let cardLink = `/decks/${deckNameParam}`;

    const deleteDeckHandler = () => {
        dispatch(deckActions.deleteDeck(name));
    }

    let factionSettings = { class: '', imgSrc: ''};

    if (faction === 'Rebel Alliance'){
        factionSettings.class = 'rebel-alliance';
        factionSettings.imgSrc = RebelAllianceWatermark;
    } else if (faction === 'Galatic Empire'){
        factionSettings.class = 'galatic-empire';
        factionSettings.imgSrc = GalaticEmpireWatermark;
    }else if (faction === 'Jedi Order'){
        factionSettings.class = 'jedi-order';
        factionSettings.imgSrc = JediOrderWatermark;
    } else {
        factionSettings.class = 'no-faction';
        factionSettings.imgSrc = NoFactionWatermark;
    }
    
    return (
        <CardWrapper>
            <CardTop src={Deck} classes={factionSettings.class}>
                <Link className='card__top-title' to={cardLink}>{name}</Link>
                <IconButton
                src={Delete}
                alt='delete'
                onClick={deleteDeckHandler}
                style={{position: 'absolute', top: '16px', right: '16px'}}
                />
                <img src={factionSettings.imgSrc} alt='faction icon' className='faction-watermark'/>
            </CardTop>
            <CardBody>
                <div className='card__body-deck'>
                    <span>{totalCards}</span>
                    <span>total cards</span>
                </div>
            </CardBody>
        </CardWrapper>
    )
}

export default CardDeck