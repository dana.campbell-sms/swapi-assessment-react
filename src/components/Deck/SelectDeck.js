import React from 'react';

import { useSelector } from 'react-redux';
import { getAllDecks } from '../../store/deck-slice';

import './SelectDeck.css';
import SelectDeckItem from './SelectDeckItem';
import Underline from '../UI/Underline';

const SelectDeck = ({cardId, page, onClickRemove, style}) => {
    const { decks } = useSelector(getAllDecks);
    let otherDecks = decks.filter(deck => !deck.cards.includes(cardId));
    let currentDeck = decks.filter(deck => deck.cards.includes(cardId));

    return (
        <div className='deck-selection' style={style}>
            <span>Select a Deck</span>
            <Underline bg={400}/>
            <div className='deck-selection__list'>
                {decks.length === 0 ? <p>There are no decks present.</p> :
                    page ==='deck-detail' ?
                    otherDecks.map((deck, i) => (
                        <SelectDeckItem title={deck.name} key={i} cardId={cardId} onClickRemove={onClickRemove} deckName={currentDeck.map(c => c.name)} page='deck-detail'/>
                    ))
                    :
                    otherDecks.map((deck, i) => (
                        <SelectDeckItem title={deck.name} onClickRemove={onClickRemove} key={i} cardId={cardId} />
                    ))
                }
            </div>
        </div>
    )
}

export default SelectDeck