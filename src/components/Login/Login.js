import React, { useState, useRef, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

import './Login.css';
import Robot from '../../assets/Robot.svg';
import User from '../../assets/User.svg';
import Password from '../../assets/Password.svg';

const Login = () => {
    const navigate = useNavigate();

    const usernameRef = useRef();

    const [username, setUsername] = useState('');
    const [pwd, setPwd] = useState('');
    // const [error, setError] = useState(second)

    useEffect(() => {
        usernameRef.current.focus()
    }, [])
    
    const submitHandler = (e) => {
        e.preventDefault();

        if(username.length === 0 || pwd.length === 0) {
            return;
        }
        window.localStorage.setItem('isLogged in', true);
        window.localStorage.setItem('username', username);
        navigate('/all-cards');
        setPwd('');
        setUsername('');
    }

    return (
        <div className='container'>
            <div className='login'>
                <div className='login-form'>
                    <img src={Robot} alt='robot' />
                    <h1>Star Wars API</h1>
                    <form onSubmit={submitHandler}>
                        <fieldset>
                            <input type='text' id='username' placeholder='Username' autoComplete='off'
                            onChange={(e) => setUsername(e.target.value)} ref={usernameRef} value={username} required/>
                            <img src={User} alt='user' />
                        </fieldset>
                        <fieldset>
                            <input type='password' id='pwd' placeholder='Password'
                            onChange={(e) => setPwd(e.target.value)} value={pwd} required />
                            <img src={Password} alt='password' />
                        </fieldset>
                        <button type='submit'>Submit</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default Login