import React from 'react';
import './NotFound.css';

const NotFound = ({title, text, imgSrc, alt}) => {
  return (
    <div className='not-found'>
        <div>
            <img src={imgSrc} alt={alt} />
            <h2>{title}</h2>
            <p>{text}</p>
        </div>
    </div>
  )
}

export default NotFound