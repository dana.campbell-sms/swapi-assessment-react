import React, { useState } from 'react';
import IconButton from '../UI/IconButton';

const FormToggler = ({src, alt, style, classes, Component, cardId, page}) => {
    const [isOpen, setIsOpen] = useState(false);

    const toggleHandler = (isOpen) => {
        setIsOpen(isOpen);
    }

    let setClassName = isOpen ? 'icon-button__active' : 'icon-button__default';

    return (
        <>
            <IconButton
                src={src}
                alt={alt}
                onClick={() => setIsOpen(!isOpen)}
                style={style} 
                classes={setClassName} />
            { isOpen &&  cardId && <Component cardId={cardId} page={page} onClickRemove={toggleHandler}/> }
            { isOpen && !cardId && <Component onClickRemove={toggleHandler} /> }
        </>
    )
}

export default FormToggler