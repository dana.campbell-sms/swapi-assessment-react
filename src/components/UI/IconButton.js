import React from 'react';
import './IconButton.css';

const IconButton = ({src, alt, onClick, style, classes}) => {
  return (
    <button className={`icon-button ${classes}`} onClick={onClick} style={style}> 
      <img src={src} alt={alt} />
    </button>
  )
}

export default IconButton