import React from 'react';
import './Underline.css';

const Underline = ({bg}) => {
    let bgColor = `var(--gray${bg})`;
    return (
        <div className='underline' style={{backgroundColor: bgColor}}></div>
    )
}

export default Underline