import React, { Fragment } from 'react';
import { useParams } from 'react-router-dom';

import { useSelector } from 'react-redux';
import { getAllCards } from '../store/card-slice';
import Card from '../components/Card/Card';
import Breadcrumb from '../components/Breadcrumb/Breadcrumb';


const CardDetail = () => {
    const { cards } = useSelector(getAllCards);
    let { cardId } = useParams();
    let singleCard = cards.filter(card => card.id === parseInt(cardId));
    let cardName = singleCard.map(card => card.cardName);

    return (
        <Fragment>
            <Breadcrumb crumb1='All Cards' crumb1Link='/all-cards' color1='var(--gray600)' crumb2={`${cardName} Details`} />

            <div style={{marginTop: '32px'}}>
                {singleCard.map((card, i) => (
                    <Card 
                        key={i}
                        id={card.id}
                        cardName={card.cardName}
                        gender={card.gender} 
                        species={card.species}
                        birth_year={card.birth_year}
                        homeworld={card.homeworld}
                        vehicles={card.vehicles}
                        starships={card.starships}
                        style={{maxWidth: '456px', minHeight: '480px'}}
                        page='card-detail'
                    />
                    ))}
            </div>
        </Fragment>
    )
}

export default CardDetail