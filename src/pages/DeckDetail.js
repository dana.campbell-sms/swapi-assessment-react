import React, { useState } from 'react';

import { useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import { getAllCards } from '../store/card-slice';
import { getAllDecks } from '../store/deck-slice';

import Card from '../components/Card/Card';
import CardList from '../components/Card/CardList';
import CardControl from '../components/Card/CardControl';
import NotFound from '../components/NotFound/NotFound';
import Robot from '../assets/Robot.svg';
import Breadcrumb from '../components/Breadcrumb/Breadcrumb';

const DeckDetail = () => {
    const { deckName } = useParams();
    const { cards } = useSelector(getAllCards);
    const { decks } = useSelector(getAllDecks);

    const [searchResults, setSearchResults] = useState([])
    const [searchTerm, setSearchTerm] = useState();
    const [termFound, setTermFound] = useState(true);
    const [activeSortOption, setActiveSortOption] = useState('');
    const [sortedData, setSortedData] = useState([])

    let selectedDeck = decks.find(deck => deck.name.toLowerCase() === deckName.replace(/-/g," ").toLowerCase());
    let cardsInDeck = selectedDeck.cards;
    let results = cards.filter(card => cardsInDeck.some(deckCard => card.id === deckCard));


    const searchTermHandler = (searchTerm) => {
        setSearchTerm(searchTerm);
        if(searchTerm !== ' ') {
            const newCardsList = results.filter((card) => {
                return Object.values(card)
                .join(' ')
                .toLocaleLowerCase()
                .includes(searchTerm.toLocaleLowerCase());
            }) 
            if (newCardsList.length === 0) {
                setTermFound(false); 
            } else {
                setTermFound(true);
                setSearchResults(newCardsList);
            }
        } 
    }

    let updatedData = searchResults.length > 0 && termFound ? searchResults : results;

    const sortCards = (option) => {
        setActiveSortOption(option);
        let sortedCards = [...updatedData].sort((cardA, cardB) => {
        // const sortedData = updatedData.sort((cardA, cardB) => {
            if (option === 'asc') {
                return cardA.cardName > cardB.cardName ? 1 : -1;   
            } 
            else if (option === 'youngest') {
                return cardA.birth_year > cardB.birth_year ? 1 : -1;
            }  
            else if (option === 'eldest') {
                return cardA.birth_year < cardB.birth_year ? 1 : -1;
            }
        });
        // console.log(sortedCards);
        setSortedData(sortedCards);
    };

    
    let cardData = sortedData.length > 0 ? sortedData : updatedData;

    return (
        <div>
            <Breadcrumb crumb1='Decks' crumb1Link='/decks' color1='var(--gray600)' crumb2={deckName.replace(/-/g," ")} />
            <CardControl 
                onSearchTerm={searchTermHandler}
                onSortCards={sortCards}
                term={searchTerm} 
                setActiveSortOption={activeSortOption} 
            />
            {
                results.length === 0 ? 
                    <NotFound imgSrc={Robot} title={`${deckName.replace(/-/g," ")} is empty`} text='Add cards to the deck' alt='robot' />
                :
                    <CardList data={cardData} term={searchTerm} termFound={termFound} Component={Card} page='deck-detail'/>
            }
        </div>
    )
}

export default DeckDetail