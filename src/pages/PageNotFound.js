import React from 'react';
import NotFound from '../components/NotFound/NotFound';
import Error404 from '../assets/404-error.svg';

const PageNotFound = () => {
  return <NotFound imgSrc={Error404} title='404 Error' text='The page you are looking for does not exist' alt='404 error' />
}

export default PageNotFound