import React, { Fragment, useState } from 'react';
import { useSelector } from 'react-redux';
import { getAllDecks } from '../store/deck-slice';

import Breadcrumb from '../components/Breadcrumb/Breadcrumb';
import CardControl from '../components/Card/CardControl';
import CardList from '../components/Card/CardList';
import CardDeck from '../components/Deck/CardDeck';
import IconButton from '../components/UI/IconButton';

import Add from '../assets/Add.svg';

const Decks = () => {
    const { decks } = useSelector(getAllDecks);
    const [results, setResults] = useState([])
    const [searchTerm, setSearchTerm] = useState();
    const [termFound, setTermFound] = useState(true);

    const searchTermHandler = (searchTerm) => {
        setSearchTerm(searchTerm);
        if(searchTerm !== ' ') {
            const newCardsList = decks.filter((card) => {
                return Object.values(card)
                .join(' ')
                .toLocaleLowerCase()
                .includes(searchTerm.toLocaleLowerCase());
            }) 
            if (newCardsList.length === 0) {
                setTermFound(false); 
            } else {
                setTermFound(true);
                setResults(newCardsList);
            }
        } 
    }

    let updatedData = results.length > 0 && termFound ? results : decks;

    return (
        <Fragment>
            <Breadcrumb crumb1='Decks' crumb1Link='/decks' crumb2='Select a Deck' color2='var(--gray500)'/>
            <CardControl 
             onSearchTerm={searchTermHandler}
             term={searchTerm} 
            deck/>
            {
                decks.length === 0 ?
                <span>
                    No Decks Created. Please create a Deck by pressing the Add Deck 
                    <IconButton
                        src={Add}
                        alt='plus sign'
                        style={{height: '20px', width: '22px', background: 'var(--white)', margin: '0 4px',
                        boxShadow: '0px 2px 4px rgba(0, 0, 0, 0.08), 0px 4px 6px rgba(0, 0, 0, 0.12'}} 
                    />
                    button above.
                </span>
                :
                <CardList data={updatedData} term={searchTerm} termFound={termFound} Component={CardDeck} />
            }
        </Fragment>
    )
}

export default Decks