import React, { Fragment, useState, useEffect } from 'react';

import { useSelector } from 'react-redux';
import { getAllCards } from '../store/card-slice';

import CardList from '../components/Card/CardList';
import CardControl from '../components/Card/CardControl'
import Card from '../components/Card/Card';
import Breadcrumb from '../components/Breadcrumb/Breadcrumb';

const AllCards = () => {
    const {cards : data, status} = useSelector(getAllCards);
    const [searchTerm, setSearchTerm] = useState();
    const [termFound, setTermFound] = useState(true);
    const [activeSortOption, setActiveSortOption] = useState('');
    const [results, setResults] = useState([])

    const searchTermHandler = (searchTerm) => {
        setSearchTerm(searchTerm);
        if (searchTerm !== ' ') {
            // if()
            const newCardsList = data.filter((card) => {
                return Object.values(card)
                .join(' ')
                .toLocaleLowerCase()
                .includes(searchTerm.toLocaleLowerCase());
            }) 
            if (newCardsList.length === 0) {
                setTermFound(false); 
                // console.log('term found', termFound)
            } else {
                setTermFound(true);
                setResults(newCardsList);
                // console.log('Results', results, termFound)
            }
        } 
        // else {
        //     setResults(data)
        // }
    }

    const sortCards = (option) => {
        setActiveSortOption(option);
        // console.log(option)
        let sortedCards = [...results].sort((cardA, cardB) => {
            if (option === 'asc') {
                return cardA.cardName > cardB.cardName ? 1 : -1;   
            } 
            else if (option === 'youngest') {
                return cardA.birth_year > cardB.birth_year ? 1 : -1;
            }  
            else if (option === 'eldest') {
                return cardA.birth_year < cardB.birth_year ? 1 : -1;
            }
        });
        setResults(sortedCards);
    };

    useEffect(() => {
        setResults(results.length > 0 && termFound ? results : data);
    }, [data, results, termFound])
    
    return (
        <Fragment>
            <Breadcrumb crumb1='All Cards' crumb1Link='/all-cards' color1='var(--gray600)' crumb2='Select a Card' color2='var(--gray500)'/>
            <CardControl 
                onSearchTerm={searchTermHandler}
                onSortCards={sortCards}
                term={searchTerm} 
                setActiveSortOption={activeSortOption}
            />
            <CardList data={results} status={status} term={searchTerm} termFound={termFound} Component={Card} page='all-cards'/>
            
        </Fragment>
    )
  
};

export default AllCards;
