import { createSlice } from '@reduxjs/toolkit';

const initialState = [
    { 
        name: 'Rebel Alliance Deck',
        faction: 'Rebel Alliance',
        cards: [],
        totalCards: 0,
    },
    {
        name: 'Galatic Empire Deck',
        faction: 'Galatic Empire',
        cards: [3, 4, 6],
        totalCards: 3,
    },
    {
        name: 'Jedi Order Deck',
        faction: 'Jedi Order',
        cards: [10, 7],
        totalCards: 2,
    },
    {
        name: 'Power Rangers No Deck',
        faction: 'none',
        cards: [2, 8],
        totalCards: 2,
    },
]

const deckSlice = createSlice({
    name: 'decks',
    initialState: {
        decks: initialState,
    },
    reducers: {
        addNewDeck: (state, {payload}) => {
            const newDeck = payload;
            const existingDeck = state.decks.find((deck) => deck.name.toLowerCase() === payload.name.toLowerCase());
            if (!existingDeck) {
            state.decks.push({
                name: newDeck.name,
                faction: newDeck.faction,
                cards: [],
                totalCards: 0
            });
            }
        },
        deleteDeck: (state, {payload}) => {
            state.decks = state.decks.filter((deck) => deck.name !== payload);
        },
        addCardToDeck: (state, {payload}) => {
            const deckName = payload.name;
            const cardId = payload.cardId;
            
            let selectedDeckIndex = state.decks.findIndex(deck => deck.name.toLowerCase() === deckName.toLowerCase());
            let existingDeckCard = state.decks.filter(deck => deck.cards.some(singleCard => singleCard === parseInt(cardId)));
            
            if (existingDeckCard.length === 0) {
                state.decks[selectedDeckIndex].cards.push(cardId);
                state.decks[selectedDeckIndex].totalCards++;
            }

        },
        deleteCardFromDeck : (state, {payload}) => {
            let deckName = payload.name;
            let cardId = payload.id;

            let selectedDeckIndex = state.decks.findIndex(deck => deck.name.toLowerCase() === deckName.toLowerCase());
            state.decks[selectedDeckIndex].cards = state.decks[selectedDeckIndex].cards.filter(card => card !== parseInt(cardId));
            state.decks[selectedDeckIndex].totalCards--;
        },
    },
    
});

export const deckActions = deckSlice.actions;
export const getAllDecks = (state) => state.decks;
export default deckSlice;