import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";

let endpoints = [
    'https://swapi.dev/api/people/?page=1&format=json',
    'https://swapi.dev/api/people/?page=2&format=json',
    'https://swapi.dev/api/people/?page=3&format=json',
    'https://swapi.dev/api/people/?page=4&format=json',
    'https://swapi.dev/api/people/?page=5&format=json',
    'https://swapi.dev/api/people/?page=6&format=json',
    'https://swapi.dev/api/people/?page=7&format=json',
    'https://swapi.dev/api/people/?page=8&format=json',
    'https://swapi.dev/api/people/?page=9&format=json',
]

export const fetchAsyncCards = createAsyncThunk(
    'cards/fetchAsyncCards',
    async () => {
        const response1 = await axios.get(endpoints[0]);
        const response2 = await axios.get(endpoints[1]);
        const response3 = await axios.get(endpoints[2]);
        const response4 = await axios.get(endpoints[3]);
        const response5 = await axios.get(endpoints[4]);
        const response6 = await axios.get(endpoints[5]);
        const response7 = await axios.get(endpoints[6]);
        const response8 = await axios.get(endpoints[7]);
        const response9 = await axios.get(endpoints[8]);
        
        let data = [
            ...response1.data.results, ...response2.data.results, ...response3.data.results,
            ...response4.data.results, ...response5.data.results, ...response6.data.results,
            ...response7.data.results, ...response8.data.results, ...response9.data.results,
        ];

        let cardsData = [];
        for (const key in data) {
            cardsData.push({
                id: parseInt(key)+1,
                cardName:data[key].name,
                gender:data[key].gender,
                birth_year:data[key].birth_year,
                species: data[key].species,
                homeworld:data[key].homeworld, 
                vehicles:data[key].vehicles, 
                starships:data[key].starships 
            });
        }
        return cardsData;
    }
)

const cardSlice = createSlice({
    name: 'cards',
    initialState: {
        cards: [],
        status : null,
    },
    reducers : {
    },
    extraReducers : {
        [fetchAsyncCards.pending]: (state) => {
            state.status = 'loading';
        },
        [fetchAsyncCards.fulfilled]: (state, {payload}) => {
            state.cards = payload;
            state.status = 'success';
        },
        [fetchAsyncCards.rejected]: (state) => {
            state.status = 'error';
        },
    },
})

export const cardActions = cardSlice.actions;
export const getAllCards = (state) => state.cards;
export default cardSlice;