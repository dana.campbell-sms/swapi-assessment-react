import { configureStore } from "@reduxjs/toolkit";
import cardSlice from "./card-slice";
import deckSlice from "./deck-slice";

const store = configureStore({
    reducer: {
        cards: cardSlice.reducer,
        decks: deckSlice.reducer
    }
})

export default store;