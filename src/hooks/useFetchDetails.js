import { useState, useEffect } from 'react';
import axios from "axios";


export default function useFetch(endpoints){
    const [cardData, setCardData] = useState([]);

    useEffect(() => {
        let data = []
        const fetchCards = async () => {
            if(endpoints.length === 0) {
                return setCardData(0)
            } else if(Array.isArray(endpoints)) {
                Promise.all(endpoints.map((endpoint) => axios.get(endpoint))).then(
                    axios.spread((...allData) => {
                        for(let i = 0; i < endpoints.length; i++) {
                            data.push(allData[i].data.name);
                        }
                        setCardData(data)
                    })
                );
            }
             else {
                const response = await axios.get(endpoints);
                setCardData(response.data.name);
            }
            
        }
        fetchCards();
    }, [endpoints]);
    
    return { cardData }
}